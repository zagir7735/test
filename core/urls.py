from django.urls import path, include
from rest_framework.routers import DefaultRouter
from core import views

router = DefaultRouter()
router.register('products', views.ProductViewSet, basename='product')
router.register('lessons', views.LessonViewSet, basename='lesson')

urlpatterns = [
    path('', include(router.urls)),
    path('stats/products/', views.ProductStatsAPIView.as_view(), name='product-stats'),
]
