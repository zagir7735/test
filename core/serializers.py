from rest_framework import serializers
from .models import Product, Lesson, Group


class ProductSerializer(serializers.ModelSerializer):
    lesson_count = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = '__all__'

    def get_lesson_count(self, obj):
        return obj.lessons.count()


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = '__all__'


class GroupSerializer(serializers.ModelSerializer):
    participant_count = serializers.SerializerMethodField()

    class Meta:
        model = Group
        fields = '__all__'

    def get_participant_count(self, obj):
        return obj.participants.count()


class ProductStatsSerializer(serializers.ModelSerializer):
    num_participants = serializers.IntegerField()
    fill_percentage = serializers.FloatField()
    purchase_percentage = serializers.FloatField()

    class Meta:
        model = Product
        fields = ['id', 'name', 'num_participants', 'fill_percentage', 'purchase_percentage']
