# Generated by Django 5.0.2 on 2024-03-03 05:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_alter_group_options_rename_mail_userprofile_email_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='participants',
            field=models.ManyToManyField(blank=True, related_name='products_participated', to='core.userprofile', verbose_name='участники'),
        ),
    ]
