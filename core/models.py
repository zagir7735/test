from django.db import models
from django.contrib.auth import get_user_model

DjangoUser = get_user_model()


class User(DjangoUser):
    class Meta:
        proxy = True


class UserProfile(models.Model):
    class Role(models.TextChoices):
        TEACHER = 'TC', 'преподаватель',
        STUDENT = 'ST', 'студент',

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='profile',
    )
    first_name = models.CharField('имя', max_length=255)
    last_name = models.CharField('фамилия', max_length=255)
    patronymic = models.CharField('отчество', max_length=255, blank=True)
    birth = models.DateField('дата рождения', null=True, blank=True)
    email = models.EmailField('адрес почты', max_length=255, blank=True)
    role = models.CharField('роль', max_length=2, choices=Role.choices, default=Role.STUDENT)
    avatar = models.ImageField('фото профиля', upload_to='profile/', blank=True)
    about_yourself = models.TextField('о себе', blank=True)

    class Meta:
        verbose_name = 'профиль пользователя'
        verbose_name_plural = 'профили пользователей'

    def __str__(self) -> str:
        return self.fullname

    @property
    def fullname(self) -> str:
        return ' '.join(filter(bool, [self.last_name, self.first_name, self.patronymic]))


class Lesson(models.Model):
    name = models.CharField('название', max_length=255)
    url = models.URLField('ссылка', max_length=255, blank=True)
    product = models.ForeignKey(
        'core.Product',
        verbose_name='продукт',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='lessons',
    )

    class Meta:
        verbose_name = 'урок'
        verbose_name_plural = 'уроки'

    def __str__(self) -> str:
        return str(self.name)


class Group(models.Model):
    name = models.CharField('название', max_length=255)
    participants = models.ManyToManyField(
        'core.UserProfile',
        verbose_name='участники',
        blank=True,
        related_name='groups'
    )
    url = models.URLField('ссылка', max_length=255, blank=True)
    product = models.ForeignKey(
        'core.Product',
        verbose_name='продукт',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='groups',
    )
    min_participants = models.PositiveIntegerField('минимальное кол-о участников', default=0)
    max_participants = models.PositiveIntegerField('максимальное кол-во участников', default=0)

    class Meta:
        verbose_name = 'группа'
        verbose_name_plural = 'группы'

    def __str__(self) -> str:
        return str(self.name)


class Product(models.Model):
    class Status(models.TextChoices):
        DRAFT = 'DR', 'черновик',
        PUBLISHED = 'PB', 'опубликован'
        CLOSED = 'CL', 'закрыт'
        MODERATING = 'MD', 'модерируется'

    name = models.CharField('название', max_length=255)
    created_by = models.ForeignKey(
        'core.UserProfile',
        verbose_name='преподаватель продукта',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='products',
    )

    date = models.DateTimeField('дата старта', null=True, blank=True)
    cost = models.DecimalField('стоимость', max_digits=10, decimal_places=2, null=True, blank=True)
    status = models.CharField('статус', max_length=2, choices=Status.choices, default=Status.DRAFT)
    participants = models.ManyToManyField(
        'core.UserProfile',
        verbose_name='участники',
        blank=True,
        related_name='products_participated',
    )

    class Meta:
        verbose_name = 'продукт'
        verbose_name_plural = 'продукты'

    def __str__(self) -> str:
        return str(self.name)
