from django.db import transaction

from rest_framework import viewsets, permissions, generics
from rest_framework.decorators import action
from rest_framework.response import Response

from core.models import Product, Lesson, User
from core.serializers import ProductSerializer, LessonSerializer, ProductStatsSerializer


class HasAccessToProduct(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.participants.filter(user=request.user).exists()


class HasAccessToLesson(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.product.participants.filter(user=request.user).exists()


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated, HasAccessToProduct]

    def distribute_users_to_groups(self, product):
        groups = product.groups.all()
        users = product.participants.all()
        group_counts = {group: group.participants.count() for group in groups}
        sorted_users = sorted(users, key=lambda user: user.groups.count())

        for user in sorted_users:
            min_group = min(group_counts, key=group_counts.get)
            if min_group.participants.count() < min_group.max_participants:
                min_group.participants.add(user)
                group_counts[min_group] += 1
            else:
                for group in groups:
                    if group.participants.count() < group.max_participants:
                        group.participants.add(user)
                        group_counts[group] += 1
                        break

        for group in groups:
            group.save()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.date_started is None:
            with transaction.atomic():
                self.distribute_users_to_groups(instance)

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @action(detail=False, methods=['GET'])
    def available_products(self, request):
        products = Product.objects.filter(status='PUBLISHED')

        serialized_products = []
        for product in products:
            serialized_product = ProductSerializer(product).data
            serialized_product['lesson_count'] = product.lessons.count()
            serialized_products.append(serialized_product)

        return Response(serialized_products)


class LessonViewSet(viewsets.ModelViewSet):
    serializer_class = LessonSerializer
    permission_classes = [permissions.IsAuthenticated, HasAccessToLesson]

    def get_queryset(self):
        queryset = Lesson.objects.all()
        product_id = self.request.query_params.get('product_id')
        if product_id:
            queryset = queryset.filter(product_id=product_id)
        return queryset

    def list(self, request, *args, **kwargs):
        product_id = request.query_params.get('product_id')
        if product_id:
            self.queryset = self.queryset.filter(product_id=product_id)
        return super().list(request, *args, **kwargs)


class ProductStatsAPIView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductStatsSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        data = []
        for product in queryset:
            num_participants = product.participants.count()
            total_capacity = 0
            total_participants = 0
            for group in product.groups.all():
                total_capacity += group.max_participants
                total_participants += group.participants.count()
            if total_capacity > 0:
                fill_percentage = (total_participants / total_capacity) * 100
            else:
                fill_percentage = 0

            total_users = User.objects.count()
            if total_users > 0:
                purchase_percentage = (num_participants / total_users) * 100
            else:
                purchase_percentage = 0

            product_data = {
                'id': product.id,
                'name': product.name,
                'num_participants': num_participants,
                'fill_percentage': fill_percentage,
                'purchase_percentage': purchase_percentage
            }
            data.append(product_data)

        return Response(data)
